lurker with mailman
-------------------

 To use lurker as external archiver for mailman's lists, both mailman and
lurker need to be configured accordingly.

 This document explains how to setup lurker and mailman in a way that
lurker archives all mailing lists that mailman provides. Though, it doesn't
cover how to split archives into public and private ones.
Read about frontends in lurkers documentation if you want to do this. Or
simply use an mda such as procmail to archive only a subset of lists.
See README.procmail or README.maildrop for more information.

-> Configuring lurker as external archiver for mailman:

 Primary, you need to add user 'list', or whoever runs mailman, to the
lurker system group. To do so, run
# adduser list lurker

 Now you have to add every mailing list that mailman knows of to lurkers
configuration file. As mailman doesn't support to use the external archiver
for only a subset of its lists, this has to cover at least all private OR
all public lists. Otherwise mail from the remaining lists will not be
archived, neither in pipermail nor in lurker. You can list all mailman lists
with the 'list_lists' command from mailman. Add every list to
/etc/lurker/lurker.conf.local or to a lurker configuration include file.

This package provides a perl script to automate the lurker configuration.
It is available at /usr/bin/mailman2lurker. The script is mainly for
creating a skeleton lurker configuration include file of mailman's list_lists
output, and for importing mailman's mbox files into the lurker database.
See the mailman2lurker(1) manpage for more information.

 Please keep in mind, that new mailman lists that lurker is not aware of yet
are not archived. lurker-index will fail as the list is not configured in
lurkers configuration yet.
In the past, this package provided a (rather ugly) wrapper for lurker-index
to save mail of new lists in a temporarely group, but that is depreciated
now due to the fact that simple regrouping of lists in lurker is not
supported any longer.

 The prefered solution now is, to keep mailman's mbox archiving, and rerun
mailman2lurker everytime you think that one or some new mailman lists exist.
For that, especially the -w option to parse [-p] and the -f option to import
[-i] are interesting. Again, see the mailman2lurker(8) for more information.

 To make mailman use lurker as its external archiver, you need to add the
following to mailmans /etc/mailman/mm_cfg.py:

---snip---
# These variables control the use of an external archiver.
# Here lurker is used as external archiver. For compatibility reasons, the
# wrapper lurker-index-lc is used instead of lurker-index. It converts the
# list name to lowercase.
PUBLIC_EXTERNAL_ARCHIVER = '/usr/bin/lurker-index-lc -l %(listname)s -m'
#PRIVATE_EXTERNAL_ARCHIVER = '/usr/bin/lurker-index-lc -l %(listname)s -m'
---snip---

 This ensures that mailman will deliver every posted and accepted mail for
public lists to lurker. as lurkers mailing list names are case sensitive,
but mailmans are case insensitive, the lurker-index-lc wrapper exists.
lurker-index-lc is a wrapper around lurker-index which converts mailing
list names to lowercase letters.

 To activate private list archiving to the same lurker database too, simply 
uncomment the second line with PRIVATE_EXTERNAL_ARCHIVE.

 Now you can change mailman's URL to mailing list archives. That will
direct the "archive" links on listinfo pages to lurkers archives.
To do so, add the following to /etc/mailman/mm_cfg.py:

---snip---
# link to lurkers list overview
PUBLIC_ARCHIVE_URL = 'http://%(hostname)s/lurker/list/%(listname)s.html'
---snip---

 Last but not least import the mailman mbox files of lists (normally found
at /var/lib/mailman/archives/private/<listname>.mbox/<listname>.mbox) into
your new lurker database. This can either be done my using mailman2lurker,
or manually by running the following for every list:

# lurker-index -l <listname> -i /path/to/<listname>.mbox

Don't mind about mails already archived in lurker but also stored in these
mailboxes. lurker automatically filters out duplicates of already archived
mails.

 -- Jonas Meurer <jonas@freesources.org>, Sun, 26 Sep 2004 16:25:11 +0200
