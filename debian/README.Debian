lurker for Debian
-----------------


-> changes to upstream:

 The configuration file lurker.conf has been split up into a static part
at /etc/lurker/lurker.conf, what contains global archive configuration,
and a local part at /etc/lurker/lurker.conf.local, intended for list
configuration and to define more include files.
You should keep your list configuration away from lurker.conf to make package
upgrades easier.

 The debian package provides lurker-index-lc, a wrapper around lurker-index.
It converts mailmans mailing list names to lowercase on the fly, as lurkers
list names are case sensitive while mailmans aren't.


-> initial setup:

 The first step to setup lurker, is to edit /etc/lurker/lurker.conf.local,
and add new lists there. See the example list configuration for more
information.

 Now you have to configure your mail system in a way that lurker imports
new list mail into its archives. There are three ways to achieve this:

a) configure your mailing list processing software to use lurker as archiver.
     - README.mailman explains how to do this with mailman
b) configure your mail delivery agent to deliver mails to lurker.
     - README.maildrop explains how to do this with maildrop
     - README.procmail explains how to do this with procmail
c) configure your mail transfer agent to deliver mails to lurker.

 Last but not least you can import the old mail archives into lurker archives.
Run 'lurker-index -l [list] -i [mbox/maildir]' for every list, where [list]
is the listname from lurker.conf and [mbox/maildir] is either a mbox file
or a Maildir directory.


-> most recent message at the top for messages sorted by date:

 To sort messages at the mindex page from top to bottom, with the most recent
messages at top, simply apply the patch at /usr/share/doc/lurker/mindex.patch
to /etc/lurker/ui/mindex.xsl.


-> the delete button:

 Since version 2.0, lurker supports a delete button in its web archives.
This button requires a password for authentication, and then markes the
corresponding mail as 'deleted' in its database. This will prevent the mail
from being listed in the web archives, even after reimporting the archives.
The password that is being asked is the password for the lurker system group.
You can set it by invoking 'gpasswd lurker'.


-> webserver:

 If you run apach2 as webserver, it may already be configured in terms of
a include file at /etc/apache2/conf-available/lurker.conf. In this case
lurker should be accessible at http://localhost/lurker

 The cgi and rewrite modules need to be enabled for lurker to work properly.
They should have been enabled by lurker package installation. In case that you
disabled them manually beforehand or afterwards, you might need to re-enable
them manually by running 'a2enmod cgi rewrite'.

 If you run another webserver than apache2, and you have suggestions or
patches to support your webserver configuration, please file a wishlist
bug against lurker in the debian bts.


-> permission management:

 To give a user write access to the db, simply add this user to the group
lurker with 'adduser [user] lurker'. /var/lib/lurker, the directory where
the database is stored, is owned by root.lurker. Group lurker has write
access to the directory.


-> see README.* files for further instructions.

 -- Jonas Meurer <jonas@freesources.org>, Wed,  7 Sep 2005 16:03:58 +0200
