lurker (2.3-6.1) unstable; urgency=medium

  * Patch for REDIRECT_* env vars.

 -- Autistici/Inventati <debian@autistici.org>  Sun, 22 Aug 2021 21:42:01 +0100

lurker (2.3-6) unstable; urgency=high

  * fix FHS violation: move default www directory from /usr/share/lurker/www
    to /var/lib/lurker/www. Thanks to Dominic Hargreaves for report and
    patch. (closes: #847751)
  * High urgency due to RC bug.
  * add dutch debconf translation by Frans Spiesschaert (closes: #767251)
  * add brazilian portuguese translation by Adriano Rafael Gomes.
    (closes: #844669)
  * debian/compat: bump debhelper compat version to 9.
  * debian/control: bump standards-version to 3.9.8, no changes required.

 -- Jonas Meurer <mejo@debian.org>  Tue, 20 Dec 2016 13:02:51 +0100

lurker (2.3-5) unstable; urgency=high

  * Acknowledge NMU. Thanks to Colin Watson. (closes: #734731)
  * Fix FTBFS, thanks to David Suárez for bugreport and Juhani Numminen for
    the patch. (closes: #753229)
  * High urgency due to serious FTBFS bug.
  * Incorporate documentation for configuring lurker with exim4. Thanks to
    Chris Knadle for writing and providing the README. (closes:  #739230)

 -- Jonas Meurer <mejo@debian.org>  Thu, 24 Jul 2014 12:20:57 +0200

lurker (2.3-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Pass "-s /bin/sh" to "su www-data" to cope with the change of www-data's
    shell in base-passwd 3.5.30 (closes: #734731).

 -- Colin Watson <cjwatson@debian.org>  Fri, 17 Jan 2014 12:35:47 +0000

lurker (2.3-4) unstable; urgency=low

  * incorporate patches from upstream svn repository:
    - 02_bump_autotools.patch: bump automake/autoconf
    - 03_nested_frontends.patch: fix a bug regarding nested frontends
  * update dependencies to 'apache2 | httpd-cgi'.
  * 01_apache.conf.patch: check for mod_rewrite before using it in apache conf.
  * patch apache.conf file to use new access control syntax for apache 2.4.
  * update apache config handling to support apache 2.4 (closes: #669814)
    - install apache include file to /etc/apache2/conf-available/lurker.conf
    - postinst/postrm scripts: enable the include file properly for 2.2 and 2.4
    - postinst script: enable cgi module as it's required by lurker
    - postinst script: enable rewrite module as it's required by lurker
    - remove debconf questions/templates regarding apache2/webserver config
  * add italian debconf translation by Beatrice Torracca (closes: #719710)
  * debian/copyright: update format to copyright-format 1.0
  * debian/control: bump standards-version to 3.9.5, no changes required

 -- Jonas Meurer <mejo@debian.org>  Fri, 13 Dec 2013 00:15:29 +0100

lurker (2.3-3) unstable; urgency=low

  * enable hardening build flags, thanks to Moritz Muehlenhoff for the patch.
    (closes: #657655)

 -- Jonas Meurer <mejo@debian.org>  Sat, 04 Feb 2012 00:11:32 +0100

lurker (2.3-2) unstable; urgency=low

  * add danish debconf translation, thanks to Joe Dalton (closes: #630347)
  * bump standards-version to 3.9.2, no changes needed
  * migrate to "3.0 (quilt)" source format

 -- Jonas Meurer <mejo@debian.org>  Mon, 20 Jun 2011 10:59:03 +0200

lurker (2.3-1) unstable; urgency=low

  * new stable upstream release:
    - fixes compiliation with gcc-4.4 (closes: #504959)
    - moves default htdocs from /var/www/lurker to /usr/share/lurker/www.
      explain the migration in NEWS.Debian
    - rewritten markup regexps (linear-time and rfc-derived) (closes: #437858)
    - remove all patches, they where all incorporated upstream.
  * drop apache from depends, it hasn't been supported in maintainer scripts
    since lurker 2.1-10
  * bump standards-version to 3.8.3, no changes needed
  * update debian/changelog to comply with proposed machine-readable format
    from http://wiki.debian.org/Proposals/CopyrightFormat
  * add ${misc:Depends} to depends in debian/control
  * set -e in postinst, preinst and prerm
  * fix coding issues with POD documentation in debian/mailman2lurker
  * add lintian-overrides for packages-contains-empty-directory. Install with
    dh_lintian, bump build-depends on debhelper to (>=6.0.7)
  * bump debian/compat to 6
  * add mindex.patch to docs and describe it's purpose in README.Debian.
  * enhance the FAQ with a description how the activity chart is calculated.
  * invoke ucf with --debconf-ok in postinst, build-depend on ucf (>= 0.28).
  * fix postinst to configure the apache2 webserver only if it's selected.
    (closes: #550647)
  * fix config maintainer script to not use absolute pathnames for binaries.

 -- Jonas Meurer <mejo@debian.org>  Sat, 31 Oct 2009 01:46:17 +0100

lurker (2.1-13) unstable; urgency=low

  * Fix pending l10n issues, thanks to Christian Perrier for coordination:
    - update japanese debconf translation, thanks to Noritada Kobayashi
      (closes: #496064)
    - update spanish debconf translation, thanks to germana (closes: #470307,
      #495861)

 -- Jonas Meurer <mejo@debian.org>  Tue, 26 Aug 2008 15:50:06 +0200

lurker (2.1-12) unstable; urgency=low

  * add russian debconf translation, thanks to Yuri Kozlov (closes: #494451)
  * remove paths from invokation of programs in $PATH in postinst and postrm.

 -- Jonas Meurer <mejo@debian.org>  Thu, 14 Aug 2008 16:01:24 +0200

lurker (2.1-11) unstable; urgency=low

  * update portuguese debconf translation, thanks to Luis Matos and Ricardo
    Silva (closes: #488579, #489810)
  * update swedish debconf translation, thanks to Martin Bagge
    (closes: #492182)
  * add finish debconf translation, thanks to Esko Arajärvi (closes: #492552)
  * bump standards-version to 3.8.0
    - Add a README.source which references /usr/share/doc/quilt/README.source.
    - Add support for debian build option parallel=n to debian/rules.

 -- Jonas Meurer <mejo@debian.org>  Tue, 29 Jul 2008 00:29:17 +0200

lurker (2.1-10) unstable; urgency=low

  * add Homepage field to debian/control.
  * fix typo in postinst script. Thanks to Reuben Thomas. (closes: #454191)
  * fix bashisms found by Raphael Geissert <atomo64@gmail.com> and his
    'archive wide checkbashisms'. (closes: #465006)
  * bump standards-version to 3.7.3 (no changes needed)
  * update gcc4.3_include_fixes.patch to include <cstring> where needed.
    Thanks to Martin Michlmayr <tbm@cyrius.com>. (closes: #456080)
  * change patch system from dpatch to quilt.
  * do some debian/rules cleanup. Many thanks to Bernhard R. Link.
  * drop support for apache, apache-perl and apache-ssl from postinst, config
    and templates. Thanks to Christian Perrier <bubulle@debian.org> for
    raising that issue.
  * drop support for upgrades from lurker < 1.0, which has never been in a
    stable release anyway.
  * update debconf template translation:
    - czech updated by Miroslav Kure (closes: #464424)
    - vietnamese updated by Clytie Siddall (closes: #464098, #464803)
    - french updated by Florentin Duneau (closes: #465648)
    - german updated by me
  * lowercase first letter of the short description in debian/control
  * add copyright information to debian/copyright. thanks to lintian

 -- Jonas Meurer <mejo@debian.org>  Fri, 29 Feb 2008 12:44:26 +0100

lurker (2.1-9) unstable; urgency=low

  * fixed watch file

 -- Jonas Meurer <mejo@debian.org>  Fri, 10 Aug 2007 15:02:30 +0200

lurker (2.1-8) unstable; urgency=low

  * added japanese debconf translations, thanks to Kobayashi Noritada
    <nori1@dolphin.c.u-tokyo.ac.jp> (closes: #413361)
  * added portuguese debconf translations, thanks to Ricardo Silva
    <ardoric@gmail.com> (closes: #416798)
  * added 02_gcc4.3_include_fixes.dpatch to fix build with gcc-4.3.
    Thanks to Martin Michlmayr <tbm@cyrius.com> (closes: #417380)
  * added check for delgroup before actually using it in postrm, it
    might be unavailable at purge time. Thanks to Michael Ablassmeier
    <abi@grinser.de> (closes: #417019)
  * update copyright file: change "program" to "package", and mention GPL
    version 2, not version 2.1 which is not available on debian systems.

 -- Jonas Meurer <mejo@debian.org>  Sat, 28 Apr 2007 02:50:14 +0200

lurker (2.1-7) unstable; urgency=low

  * updated vi debconf translations, thanks to Clytie Siddall
    <clytie@riverland.net.au>
  * fixed typo in one template (that you would like, not that
    would you like), thanks as well to Clytie Siddall
    - unfuzzied all translations

 -- Jonas Meurer <mejo@debian.org>  Wed, 20 Dec 2006 05:05:31 +0100

lurker (2.1-6) unstable; urgency=medium

  * update czech debconf translations, thanks to Miroslav Kure
    <kurem@upcase.inf.upol.cz> (closes: #389214)
  * update swedish debconf translations, thanks to Daniel Nylander
    <yeager@lidkoping.net>
  * don't do anything for webserver "none". this option was present in
    the sarge versions of the package. urgency medium due to this bugfix.
    thanks to Christoph Berg. (closes: #397472)
  * bump dh_compat to 5
  * remove code to enable the apache rewrite module. really warn and stop
    automatic webserver configuration if the module is not enabled.

 -- Jonas Meurer <mejo@debian.org>  Tue, 21 Nov 2006 00:48:57 +0100

lurker (2.1-5) unstable; urgency=low

  * fix typo in README.Debian, it's called lurker-index-lc, not
    lurker-index-fc. Thanks to Julian Mehnle <julian@mehnle.net>
  * bump standards-version to 3.7.2.0, no changes needed
  * add a debconf template to set the lurker system group password
  * depend on passwd (>= 1:4.0.16-1) as we use chgpasswd in postinst
  * update french debconf template translations, thanks to Luc FROIDEFOND
    and Florentin Duneau (closes: #360150, #369266)
  * update swedish debconf translations, thanks to Daniel Nylander
    <po@danielnylander.se>
  * update czech debconf translations, thanks to Miroslav Kure
    <kurem@upcase.inf.upol.cz>
    <f.baced@wanadoo.fr>
  * use groupdel instead of delgroup in postrm, as adduser is not essential.
    Thanks to Bill Allombert. (closes: 388660)

 -- Jonas Meurer <mejo@debian.org>  Fri, 22 Sep 2006 03:05:58 +0200

lurker (2.1-4) unstable; urgency=low

  * postinst: strip the comma from a debconf multiselect item. Thanks to
    Paul Wise <pabs3@bonedaddy.net> (closes: #356978)
  * postinst: don't give 'none' as an option for the lurker/apache_conf
    debconf multiselect. this cleans up the code and fixes a false positive
    when both apaches and 'none' are selected.

 -- Jonas Meurer <mejo@debian.org>  Mon, 20 Mar 2006 21:28:45 +0100

lurker (2.1-3) unstable; urgency=low

  * add build-depends on dpatch, thanks to Bastian Blank <waldi@debian.org>
    (closes: #356935)

 -- Jonas Meurer <mejo@debian.org>  Wed, 15 Mar 2006 17:52:38 +0100

lurker (2.1-2) unstable; urgency=low

  * release 2.1-1 fixed the following security issues:
    - Since the configuration file needs to be specified in the URL and
      lines not understood are exposed in an error message lurker was
      able to display all files that are readable for the www-data user
      and group. (CVE-2006-1062)
    - It is possible for a remote attacker to create or overwrite files
      in any writable directory that is named "mbox". (CVE-2006-1063)
    - Missing input sanitising allows an attacker to inject arbitrary
      web script or HTML. (CVE-2006-1064)
  * rename luker-index-mm to lurker-index-lc. drop support for automatical
    list configuration for new lists. update documentation accordingly.
  * completely rewrite mailman2lurker.pl, rename it to mailman2lurker.
    install mailman2lurker into /usr/bin, provide a manpage.
  * add a note about the delete button in README.Debian.
  * add patches/01_umask.dpatch, which adds the possibility to configure the
    umask for lurker-index and lurker-search in lurker.conf. it fixes also the
    documentation to not suggest to invoke lurker-index via 'sg' any more.

 -- Jonas Meurer <mejo@debian.org>  Tue, 14 Mar 2006 13:26:58 +0100

lurker (2.1-1) unstable; urgency=low

  * new upstream release, fixing some security issues
  * run lurker-prune only in prerm, not in postrm. Thanks, Wesley
  * run lurker-prune in postinst too. Thanks, Wesley
  * rewrite mailman2lurker script
  * minor improvements to README.mailman, still a major revision is needed
  * add --build (and eventually even --host) to confflags for cross-compiling
  * update README.procmail to use group lurker for running lurker-index

 -- Jonas Meurer <mejo@debian.org>  Tue,  7 Mar 2006 17:51:36 +0100

lurker (2.0-1) unstable; urgency=low

  * new upstream release
    - use default config file for all binaries, remove lurker-index wrapper
    - lurker-prune doesn't support -d any more
    - adjust apache.conf
    - the webinterface now has a reply and a trash button
    - multiple frontends are supported now
  * adjusted the configure flags
    - cgi scripts are now in /usr/lib/cgi-bin/lurker/
  * removed old-mailman2lurker.sh
  * updated 'database regeneration' debconf template and de.po
  * don't remove conf.d/lurker for unconfigured apaches
  * ran ispell against all files in debian, fixed many typos.

 -- Jonas Meurer <mejo@debian.org>  Tue, 28 Feb 2006 21:11:19 +0100

lurker (1.3-6) unstable; urgency=low

  * fix permission of /var/www/lurker/mindex
  * run lurker-prune as user www-data in prerm
  * fix instructions about integration with mailman, thanks to
    Mihai Maties <mihai@xcyb.org> (closes: #347896)
  * run lurker-params only if available, fallback to sed otherwise.
    Thanks to Laurent Bonnaud <bonnaud@lis.inpg.fr> (closes: #348643)

 -- Jonas Meurer <mejo@debian.org>  Fri, 20 Jan 2006 12:29:21 +0100

lurker (1.3-5) unstable; urgency=low

  * added depends on adduser (>= 3.11)
  * fixed systemgroup 'lurker' creation and removal
  * improved README.procmail, thanks to Seb <seb@h-k.fr> for the
    inspirations
  * added swedish debconf translation, thanks to Daniel Nylander
    <yeager@lidkoping.net>. (closes: #342653)
  * build against libmimelib1c2a (closes: #344490)

 -- Jonas Meurer <mejo@debian.org>  Tue, 27 Dec 2005 17:24:50 +0100

lurker (1.3-4) unstable; urgency=low

  * improved README.Debian:
    - lurker-regenerate is not a change to upstream
    - apache2 doesn't enable the rewrite module by default.
    - make configuration setup more clear
    - improve the order of sections
  * check for rewrite module before automaticly configuring apache2.
    don't configure apache2 in case that the module isn't enabled, but
    output an error message instead. (closes: #326924)
  * relicensed lurker-regenerate(1) and lurker-index-mm(1) manpages to
    remove GFDL licensed documentation. GFDL is not DFSG-free.
  * improve lurker <-> mailman configuration
    - lurker-index-mm checks for configured list before checking file
      permissions
    - /var/lib/lurker/* needs group writable permissions
  * suggest gnupg, as lurker uses it to verify pgp/gpg keys
  * suggest mailman, in most cases a list management software and the archiver
    are on the same host

 -- Jonas Meurer <mejo@debian.org>  Wed, 26 Oct 2005 15:57:01 +0200

lurker (1.3-3) unstable; urgency=low

  * rebuild against libstdc++6 and libmimelib1c2 for transitions
    unfortunately libmimelib1c2 depends on kdelibs4c2 what causes
    many new depends for lurker, especially when installed on a box without
    x11 and kde. I'll try to unfuzzy that in future.
  * add -I/usr/include/qt3 to CXXFLAGS to workaround the now broken configure
    script which does not include qt3 headers (mimelib didn't depend on them,
    that's new with kde 3.4.2).
  * check for /usr/bin/ucf before using it in postrm
  * add debconf-2.0 as alternative for debconf in depends
  * unfuzzied debconf translations
  * fixed bashims and usage of chmod in maintainer scripts
  * removed obsolete support for upgrades from versions before 1.0
  * updated address of the FSF in debian/copyright
  * fixed some typos in debconf templates, thanks to Clytie Siddall
    (closes: #312624)
  * added vietnamese debconf templates, thanks to Clytie Siddall
    (closes: #312623)
  * updated german debconf templates, thanks to Jens Seidel
    (closes: #313973)

 -- Jonas Meurer <mejo@debian.org>  Sun,  4 Sep 2005 05:28:20 +0200

lurker (1.3-2) unstable; urgency=low

  * patched common/MessageId.cpp to make lurker compile with gcc-2.95
  * updated french debconf templates, thanks to Eric (closes: #308354)

 -- Jonas Meurer <mejo@debian.org>  Tue, 10 May 2005 16:17:45 +0200

lurker (1.3-1) unstable; urgency=low

  * new bugfix-only upstream release 
  * updated czech debconf templates, thanks to Miroslav Kure
    (closes: #307169)

 -- Jonas Meurer <mejo@debian.org>  Wed,  4 May 2005 13:52:15 +0200

lurker (1.2-6) unstable; urgency=low

  * patch lurker/CharsetEscape.cpp: lurker parsed mutt-generated
    subject headers incorrectly (closes: #306859)
  * add a note in NEWS.Debian to inform admins about the fact that
    they have to regenerate lurkers database to apply the parsing
    fix.
  * updated french debconf templates, thanks to Eric (closes: #298958)

 -- Jonas Meurer <mejo@debian.org>  Fri, 29 Apr 2005 15:47:28 +0200

lurker (1.2-5) unstable; urgency=low

  * added czech debconf translation, thanks to Miroslav Klose
    (closes: #288017)
  * cleaned up maintainer scripts
    - moved #DEBHELPER# to beginning of postrm, to place it before rm -rf
    - fixed lurker apache config removal
    - fixed postinst to remove obsolete apache configs at dpkg-reconfigure
    - added prerm: run lurker-prune to clean cache if db exists
    - moved to tempfile for save generation of temporary conffile
    - fixed ucf calls, lurker.conf now really maintained by ucf
    - added apache2 support at the automatic webserver configuration
  * split lurker.conf into lurker.conf.template and lurker.conf.local at build
    - keep lurker.conf clean of list configuration to make maintainer scripts
      more happy with the configuration files
  * improved documentation
    - some proofreading of README.procmail and README.maildrop
    - some proofreading and reorganization of README.Debian
    - some proofreading and improvements of README.mailman (closes: #294130)

 -- Jonas Meurer <mejo@debian.org>  Sun, 27 Feb 2005 15:50:26 +0100

lurker (1.2-4) unstable; urgency=low

  * rebuild against latest libmimelib1a

 -- Jonas Meurer <mejo@debian.org>  Mon, 22 Nov 2004 01:27:18 +0100

lurker (1.2-3) unstable; urgency=low

  * the "what have i done?" release
  * built in up-to-date sid to make autobuilders happy
  * improved README.mailman

 -- Jonas Meurer <mejo@debian.org>  Sun, 26 Sep 2004 16:27:51 +0200

lurker (1.2-2) unstable; urgency=high

  * rebuilt in sarge: libmimelib from unstable kept lurker out of sarge
  * urgency high to push it into sarge

 -- Jonas Meurer <mejo@debian.org>  Tue, 21 Sep 2004 19:03:03 +0200

lurker (1.2-1) unstable; urgency=low

  * new upstream version
  * removed deutsch.patch (merged upstream)
  * removed lurker-regenerate.1 manpage (merged upstream)
  * added information about the regroupable option in README.Debian 

 -- Jonas Meurer <mejo@debian.org>  Mon, 30 Aug 2004 18:11:44 +0200

lurker (1.1-6) unstable; urgency=low

  Thanks to Christian Perrier:

  * updated French debconf translation (closes: #257549)
  * patched german xsl template (closes: #261206)
  * (hopefully) fixed debian/watch
  * added a mailman2lurker rewrite in perl. much better code! 

 -- Jonas Meurer <mejo@debian.org>  Sun,  1 Aug 2004 17:05:17 +0200

lurker (1.1-5) unstable; urgency=low

  * removed libmimelib-dev from build-depends, as only libmimelib1-dev exists
  * really installing README.mailman & README.maildrop to doc directory
  * added mailman2lurker.sh script, written in bash. it really needs to be
    rewritten in perl as soon as possible. the code is terrible!

 -- Jonas Meurer <mejo@debian.org>  Sun,  4 Jul 2004 03:05:41 +0200

lurker (1.1-4) unstable; urgency=low

  * moved xsl stuff (fmt directory) to /etc/lurker (closes: #254470) 

 -- Jonas Meurer <mejo@debian.org>  Fri, 18 Jun 2004 22:08:11 +0200

lurker (1.1-3) unstable; urgency=low

  * fixed lurker.conf.mm (heading was to long) 

 -- Jonas Meurer <mejo@debian.org>  Wed,  9 Jun 2004 16:54:39 +0200

lurker (1.1-2) unstable; urgency=low

  * Added French debconf translation (closes: #246807)
  * Added debian/watch file
  * Improved and fixed lurker-index-mm
    - uses /etc/lurker/lurker.conf.mm now for creating new lists
    - removed annoying debugging stuff
    - allow to configure default listname in lurker.conf.mm
    - allow to configure default list url in lurker.conf.mm
  * Updated README.Mailman
  * added examples for lurker.conf and lurker.conf.mm in docs/lurker/examples 

 -- Jonas Meurer <mejo@debian.org>  Tue,  8 Jun 2004 23:50:34 +0200

lurker (1.1-1) unstable; urgency=low

  * improved and redesigned maintainer scripts:
    - Modified check for older databases
    - improved debconf style (check for files, scripts break more rarely)
    - Handling lurker.conf with ucf now
    - Updated to support new apache config style
    - Check for apaches include dirs, if failed don't ask for update but
      output a different debconf template.
  * Acknowledged NMUs (closes: #200872, #233900, #235879, #240865)
  * changed Build-Depends to libmimelib1-dev | libmimelib-dev.
  * added po-debconf to Build-Depends
  * added README.mailman and lurker-index-mm, support for mailman

 -- Jonas Meurer <mejo@debian.org>  Fri, 21 May 2004 17:31:22 +0200

lurker (1.1-0.2) unstable; urgency=low

  * Rebuilt against libmimelib1-dev (closes: #240865)
  * Include default config file (closes: #235879)
  * Patch to add new translations (closes: #233900)
  * Include a backup copy of the default lurker config in doc
  * Fixed a problem with apache not restarting after lurker install

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Mon,  5 Apr 2004 18:51:23 +0200

lurker (1.1-0.1) unstable; urgency=low

  * New upstream release (NMU by request of maintainer)
   - removed patches from 1.0-0unofficial (fixed upstream)

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Wed, 18 Feb 2004 15:22:54 +0100

lurker (1.0-0.2) unstable; urgency=low

  * Rebuilt against the current sid tree

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Fri,  5 Dec 2003 11:57:43 +0000

lurker (1.0-0.1) unstable; urgency=low

  * NMU by request of maintainer (closes: #200872)

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Fri,  5 Dec 2003 12:40:26 +0100

lurker (1.0-0unofficial) unstable; urgency=low

  * Unofficial debian packages for 1.0
  * Changed the rules file to use make
  * Removed build-depends on jam and xsltproc
  * Updated to standards-version 3.6.1.0 (no changes)
  * Set act_ver in lurker.postinst to 1.0 so an automatic upgrade happens
  * Move converted config file out of the way after auto-upgrade
  * Work around known 1.0 bugs:
   - Create missing mbox directory
   - set the dbdir config setting
   - use gzip -dfc for lurker-regenerate
   - Tweak ui/common.xsl to have spaces in verion and admin-by
   - Tweak ui/splash.xsl to sort by list id

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Thu, 13 Nov 2003 22:19:19 +0100

lurker (0.9-1) unstable; urgency=low

  * new upstream release (adds lurker-list and lurker-params to facilitate
    script writing, is used for debconf code in debian scripts now)
  * fixed the permissions of /var/www/lurker/list

 -- Jonas Meurer <mejo@debian.org>  Sat, 14 Jun 2003 16:38:59 +0200

lurker (0.8-5) unstable; urgency=low

  * added README.maildrop
  * upgraded the standards version
  * fixed sed usage in automatic lurker.conf upgrade

 -- Jonas Meurer <mejo@debian.org>  Tue,  3 Jun 2003 19:38:41 +0200

lurker (0.8-4) unstable; urgency=low

  * fixed the upload templates (some spelling mistakes)
    made a new release to upload it although 0.8-3 is already in accepted

 -- Jonas Meurer <mejo@debian.org>  Fri, 23 May 2003 16:41:37 +0200

lurker (0.8-3) unstable; urgency=low

  * fixed the 'remove apache include' bug (closes: #194427)
  * fixed the upgrade message, will no longer be printed at first install
    (closes: #194428)
  * fixed the sed expression in postinst (closes: #194440)

 -- Jonas Meurer <mejo@debian.org>  Fri, 23 May 2003 15:46:29 +0200

lurker (0.8-2) unstable; urgency=low

  * fixed cronjob (uses lurker-prune now) (closes: #194264)
  * fixed script to configure lurker with debconf (closes: #194267)
  * fixed upgrade stuff (closes: #194276)
  * lurker-regenerate is executable, has a manpage and is in /usr/bin now
  * fixed the apache include bug (closes: #194266) 

 -- Jonas Meurer <mejo@debian.org>  Thu, 22 May 2003 18:41:41 +0200

lurker (0.8-1) unstable; urgency=low

  * new upstream release (closes: #192378, #194069)
  * added automatic database upgrade
  * removed many senceless code in postinst and postrm, wwwconfig-common usage
    is smarter now
  * automatic config file upgrade

 -- Jonas Meurer <mejo@debian.org>  Wed, 21 May 2003 23:30:09 +0200

lurker (0.7-1) unstable; urgency=low

  * new upstream release (closes: #194071)
  * fixed rules and the configure prompt
  * /var/lib/lurker is g+ws now
  * added upstream manpage
  * moved cgis to /usr/lib/cgi-bin
  * added a lurker-index wrapper for config-file and umask 

 -- Jonas Meurer <mejo@debian.org>  Sun, 27 Apr 2003 18:08:02 +0200

lurker (0.6-2) unstable; urgency=low

  * NMU to quickly address some issues that were missed
  * SymLinksIfOwnerMatch added to /etc/lurker/apache.conf -> nothing works without this
  * /var/lib/lurker should be g+w
  * -lssl in rules leads to linking against ssl which is not a dependency
  * check for group before addgroup

 -- Wesley W. Terpstra (Debian) <terpstra@debian.org>  Tue, 22 Apr 2003 23:29:24 +0200

lurker (0.6-1) unstable; urgency=low

  * new upstream release (closes: #184933, #187507)
  * fixed debian/lurker.config (closes: #164368)
  * near-complete lurker rewrite in C++
  * new db format
  * new config file format
  * no deamon any longer, cronjob is needed

 -- Jonas Meurer <mejo@debian.org>  Mon, 21 Apr 2003 17:56:14 +0200

lurker (0.1f-5) unstable; urgency=low

  * fixed depends and built against a new libc-client (closes: #182713)

 -- Jonas Meurer <mejo@debian.org>  Sun, 23 Mar 2003 20:54:21 +0100

lurker (0.1f-4) unstable; urgency=low

  * fixed depends (closes: #181149) 

 -- Jonas Meurer <mejo@debian.org>  Thu, 20 Feb 2003 13:03:43 +0100

lurker (0.1f-3) unstable; urgency=low

  * added K-link rc{0,6}.d (closes: #173406)
  * fixed depends and configure-run (closes: #171299) 

 -- Jonas Meurer <mejo@debian.org>  Thu,  9 Jan 2003 17:58:19 +0100

lurker (0.1f-2) unstable; urgency=low

  * fixed debconf stuff

 -- Jonas Meurer <mejo@debian.org>  Sat, 23 Nov 2002 12:39:29 +0100

lurker (0.1f-1) unstable; urgency=low

  * new upstream release
  * fixed lurker.mbox (now path to pidfile is correct) 

 -- Jonas Meurer <mejo@debian.org>  Wed, 30 Oct 2002 21:16:44 +0100

lurker (0.1e-3) unstable; urgency=low

  * improved init script. Now start restarts if lurkerd is already running.
    That's better for dpkg-reconfigure lurker (now it doesn't break down at
    start).
  * changed the limit of 30 in the init script to 900. Now lurkerd has enough
    time to sync it's database.

 -- Jonas Meurer <mejo@debian.org>  Sun, 20 Oct 2002 14:38:53 +0200

lurker (0.1e-2) unstable; urgency=low

  * fixed policy version in control
  * fixed debconf usage 

 -- Jonas Meurer <jonas@freesources.org>  Sun, 22 Sep 2002 13:12:49 +0200

lurker (0.1e-1) unstable; urgency=low

  * new upstream version
  * fixed init.d-script: lurker has time to sync its db at stop now
  * fixed init.d-script: now it gives more information
  * fixed conffile: /etc/lurker/apache.conf is also a conffile
  * fixed conffile: /etc/init.d/lurker is now also a conffile
  * fixed manpages
  * lurker.conf is handled by debconf now
  * fixed postinst
  * removed the question at purge. If you purge something, you want all the
    files to be removed. Otherwise you would just remove the package.
  * fixed purge process 
  * applied upstream bugfix for SIGPIPE in 0.1f
  * applied upstream bugfix for overly long caching in 0.1f

 -- Jonas Meurer <jonas@freesources.org>  Tue,  3 Sep 2002 22:38:37 +0200

lurker (0.1d-1) unstable; urgency=low

  * new upstream version

 -- Jonas Meurer <jonas@freesources.org>  Wed, 24 Jul 2002 11:30:08 +0200

lurker (0.1a-2) unstable; urgency=low

  * fixed lurker.postrm, now the purge procedure works
  * corrected description, it's a archive tool, not a archive (closes: #150882)
  * set architecture to any, builds on more than i386 (closes: #150793) 

 -- Jonas Meurer <jonas@freesources.org>  Tue, 25 Jun 2002 07:39:16 +0200

lurker (0.1a-1) unstable; urgency=low

  * first debian unstable release (closes: #149026)
  * fixed README.Debian
  * first upstream release
  * lurkerd runs now from init.d script
  * lurkerd has a manpage now

 -- Jonas Meurer <jonas@freesources.org>  Wed, 19 Jun 2002 10:00:36 +0200

lurker (0.1-2) unstable; urgency=low

  * corrected build-depends and depends
  * fixed copyright file

 -- Jonas Meurer <jonas@freesources.org>  Wed,  5 Jun 2002 10:11:45 +0200

lurker (0.1-1) unstable; urgency=low

  * Initial Release.
  * please see the BUGS file.

 -- Jonas Meurer <jonas@freesources.org>  Tue,  4 Jun 2002 20:24:09 +0200

