README.exim4                                                       Feb 16 2014

This is a step-by-step guide for using lurker with Exim4.

Steps:

   1.  Configure lurker in /etc/lurker/lurker.conf.local for your mailing
       list(s) as is explained in the README.Debian file.  For illustration
       purposes we'll assume there's an email list named 'test-list'.

   2.  A user must be added to the 'lurker' user group to allow access to
       /var/lib/lurker database files during message delivery to lurker.
       Adding Debian-exim to the 'lurker' group is one way to do this, but
       can be any other user that you want -- just set the user to use
       during message delivery in the Exim4 lurker_transport section later.
       e.g.: 'adduser Debian-exim lurker'

   3.  Choose a unique domain to use with lurker, which will be used to route
       mail from Exim4 to lurker and used for a secret email address signed up
       to mailing lists that lurker will index.
       e.g. lurk.example.net

   4.  Modify Exim4 configuration

       In the 'main' configuration section:

          # If using Debian's Exim4 setup, you could add lurk.example.net to
          # the list of domains to the dc_other_hostnames in
          # /etc/exim4/update-exim4.conf.conf instead of here if desired
          MAIN_LOCAL_DOMAINS = <other domains> : lurk.example.net
          domainlist lurker_domains = lurk.example.net
          LURKER_HOME = /var/lib/lurker

       In the 'router' section, add a lurker_router and modify the
       system_aliases router to avoid luker_domains:

          lurker_router:
            debug_print = "R: lurker_router for $local_part@$domain"
            driver = redirect
            domains = +lurker_domains
            allow_fail
            allow_defer
            data = ${lookup{$local_part}lsearch{/etc/aliases-list}}
            headers_remove = Delivered-To:Return-Path
            pipe_transport = lurker_transport

         ## Only need to modify 'domains' line:
         system_aliases:
           debug_print = "R: system_aliases for $local_part@$domain"
           driver = redirect
           domains = +local_domains : !+lurker_domains
           ...

       In the 'transport' section, add a lurker_transport.  The "user ="
       setting is not required if you used the Debian-exim user in step 2,
       otherwise set this to the user that is in the 'lurker' user group.

         lurker_transport:
           driver = pipe
           # Change Return-Path: to hide the email address used to send mail to lurker
           return_path = $header_To:
           # user = Debian-exim  ## or set to another user with access to 'lurker' group
           group = lurker
           home_directory = LURKER_HOME
           current_directory = LURKER_HOME
           # Limits allowed pipe commands in /etc/aliases to just lurker-index
           allow_commands = /usr/bin/lurker-index
       
   5.  Add an email alias to /etc/aliases for the 'lurker-discussion' list to
       pass mail to lurker-index:

           test-list-lurk: "|/usr/bin/lurker-index -l test-list -m"

   6.  Add email address test-list-lurk@lurk.example.net to mailing list.
       e.g. for mlmmj:

          /usr/bin/mlmmj-sub -a test-list-lurk@lurk.example.net \
              -L /var/spool/mlmmj/test-list -q

   7.  Test your setup with

       $ exim4 -bt test-list-lurk@lurk.example.net
       R: lurker_router for nyccug-test@lurk.coredump.us
       test-list-lurk@lurk.example.net -> |/usr/bin/mlmmj-receive -L /var/spool/mlmmj/test-list/
         transport = lurker_transport

       If you don't see "transport = lurker_transport", run the command again
       with -d to see more detail and find what's going wrong.
       Otherwise, you're done!

 -- Christopher Knadle <Chris.Knadle@coredump.us>  Sun, 16 Feb 2014 15:00:25 -0500
